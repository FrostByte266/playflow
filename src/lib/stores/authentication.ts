import { writable } from "svelte/store"
import type { IEmployee } from "$lib/types/employee"
import axios from "$lib/axios"
import Codes from "http-status-codes"
import decode from "jwt-decode"

type AuthResponse = {
    token: string
    user: IEmployee
}

type AuthStoreValue = AuthResponse | null

type SubscribeFunc = ReturnType<typeof writable<AuthStoreValue>>["subscribe"]

type AuthStore = {
    subscribe: SubscribeFunc
    signIn: (id: number, pin: string) => Promise<void>
    signOut: () => Promise<void>
}

function authStore(): AuthStore {
    let storedToken: string | null = ''
    let decoded: IEmployee | null = {id: 0, name: '', role: '', pin: ''}
    if (typeof localStorage !== 'undefined') {
        storedToken = localStorage.getItem("token")
        decoded = storedToken ? decode<IEmployee>(storedToken) : null
    }
    const { subscribe, set } = writable<AuthStoreValue>(
        storedToken && decoded ? { token: storedToken, user: decoded } : null
    )

    return {
        subscribe,
        async signIn(id, pin) {
            console.log({id, pin})
            console.dir(axios)
            const { status, data } = await axios.post<AuthResponse>(
                "/api/session",
                {
                    id,
                    pin,
                }
            )

            if (status === Codes.OK) {
                set(data)
            } else {
                throw new Error("Invalid credentials")
            }
        },
        async signOut() {
            await axios.delete("/api/session")
            set(null)
        },
    }
}

export default authStore()
