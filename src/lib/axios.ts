/* eslint-disable no-var */
import axiosLib from 'axios'

// Declare axios inside a global scope to prevent
// creating many connections during development
// due to HMR

var axios = globalThis.axios || axiosLib.create({
    withCredentials: true
})

if (process.env.NODE_ENV === 'development') globalThis.axios = axios

export default axios