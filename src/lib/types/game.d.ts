import type { IEmployee } from './employee'
import type * as Issue from './enums/issue'

export interface IGame {
    name: string,
    playerCount: number,
    manufacturer: string,
    photo: string,
    cardTapPosition: number,
    id: number
}

export interface IGameIssue {
    game: IGame,
    id: string,
    description: string,
    reportedBy: IEmployee,
    date: string,
    type: Issue.Type,
    state: Issue.Status
}
